/* 
 * File:   	MHW.h
 * Authors:	Jorge Felipe Grybosi
 *		Adan Kvitschal	
 *
 * Created on 01 de Outubro de 2018
 */

#ifndef MHW_H
#define	MHW_H

#include <stdint.h>

#include "utils/BasicTypes.h"

#include "SystemDefinitions.h"

#include "wireless/SimpleLink.h"
#include "network/MACAddress.h"

#include "control/Channel.h"


////////CHANNEL DEFINITIONS /////////////////
#define MHW_NUM_FREQUENCIES					32
#define MHW_NUM_TIMESLOTS 					20
#define MHW_TIMESLOT_PERIOD              	50
#define MHW_CONTROL_PACKET_TIME				10
#define MHW_DATA_PACKET_MAX_DELAY           30
//#define MHW_MAX_TIME_PHASE                  0x01000000 //?
#define MHW_MAX_TIME_PHASE                  0x01400000 //5ms
//#define MHW_MAX_TIME_PHASE                  0x02800000 //10ms
//#define MHW_MAX_TIME_PHASE                  0x05000000 //20ms

////// SLOT OPERATION DEFINITIONS ///////////
#define MHW_SLOT_OPERATION_IDLE				0x00
#define MHW_SLOT_OPERATION_SNIFF 			0x01
#define MHW_SLOT_OPERATION_UNICAST_RECEIVE  0x02
#define MHW_SLOT_OPERATION_UNICAST_SEND  	0x03
#define MHW_SLOT_OPERATION_BROADCAST	    0x04

///////////MHW STATES//////////////////////////
#define MHW_STATE_IDLE                      	0x00
#define MHW_STATE_INITIALIZING              	0x01
#define MHW_STATE_NEXT_CHANNEL					0x02
#define MHW_STATE_TRANSMITTING_CONTROL_PACKET	0x03
#define MHW_STATE_WAITING_CONTROL_PACKET		0x04
#define MHW_STATE_RECEIVING_CONTROL_PACKET		0x05
#define MHW_STATE_TREATING_CONTROL_PACKET       0x06
#define MHW_STATE_TRANSMITTING_DATA_PACKET		0x07
#define MHW_STATE_WAITING_DATA_PACKET			0x08
#define MHW_STATE_RECEIVING_DATA_PACKET 		0x09


/////////// BUFFER STATES /////////

#define MHW_BUFFER_STATE_FREE					0
#define MHW_BUFFER_STATE_WAITING_ASSIGNMENT		1
#define MHW_BUFFER_STATE_ASSIGNED				2
#define MHW_BUFFER_STATE_WAITING_ACK			3
#define MHW_BUFFER_STATE_WAITING_READ			4

#define MHW_FREE_SLOT_LIST_SIZE                 3

struct Transceiver;
struct Stream;


struct MHWChannelInfo {
    
	MACAddress address;

	struct {
		uint8_t index;
		uint8_t frequency;
		uint8_t timeToLive;
        uint32_t phase;

//        int32_t timestampFracPhase;
//        uint32_t rxFrac;
//        uint32_t cpFrac;
        
	} slots[MHW_MAX_CHANNEL_SLOTS];

};


struct MHWDataBuffer {
    
	uint8_t state;

	uint8_t slotIndex;

	uint32_t timeout;

    MACAddress address;
    
    struct {
        uint8_t header[8];
        uint8_t data[MHW_MTU_SIZE];
    };
  
    uint16_t dataOffset;
    uint16_t dataSize;
};

struct MHWFreeSlotData {
    
    uint8_t index;
    uint8_t frequency;
    uint8_t timeSinceLastUpdate;
    
};

struct MHWSlotData {

	uint8_t  	operation;

	uint8_t  	frequency;

	uint32_t 	totalRxBytes;

	uint8_t		attempts;
	int8_t	 	lastRSSI;
	uint8_t		timeToLive;
	uint8_t 	dataRate;
    
    uint8_t     nextSlotIndex;

	MACAddress	remoteAddress;

	struct MHWDataBuffer *dataBuffer;

};


struct MHWInstance {

	struct SLRInstance *slr;

	MACAddress mac;
    
	uint8_t state;

	uint8_t operation;
    
    uint8_t longSniff;
    
	uint32_t timestampInt;
	uint32_t timestampFrac;

    uint32_t rxStartTimestampFrac;
    uint32_t txStartTimestampFrac;
    
    uint8_t phaseUpdateDizimationCounter;
    
	uint8_t currentSlotIndex;
	//uint8_t lastSlotIndex;

    uint8_t firstOpenSlotIndex;
    
	uint8_t numIdleSlots;
	uint8_t numSniffSlots;
	uint8_t numOpenRxSlots;
	uint8_t numUnicastRxSlots;
	uint8_t numUnicastTxSlots;
	uint8_t numBroadcastSlots;

	struct MHWSlotData slot[MHW_NUM_TIMESLOTS];

	struct MHWDataBuffer txBuffers[MHW_NUM_TX_BUFFERS];

	struct MHWDataBuffer rxBuffers[MHW_NUM_RX_BUFFERS];
    
    struct Channel          channels[MHW_MAX_CHANNELS];
    struct MHWChannelInfo   channelInfo[MHW_MAX_CHANNELS];

    struct MHWFreeSlotData  freeSlots[MHW_FREE_SLOT_LIST_SIZE];
    
	uint8_t cpBuffer[30];
    
    struct MHWDataBuffer *currentRxBuffer;
    
    uint8_t messageSizeReceived;
};


void MHW_initialize(struct MHWInstance *pInstance, 
                    struct SLRInstance *pSLRInstance,
                    struct Transceiver *pTransceiver);

void MHW_run(struct MHWInstance *pInstance);

void MHW_updateTimers(struct MHWInstance *pInstance, uint32_t pElapsedTime);

BasicReturnCode MHW_send(	struct MHWInstance *pInstance,
							MACAddress pAddress,
							uint8_t *pData,
							uint16_t pDataSize);

uint8_t MHW_getNumberOfReceivedPackets(struct MHWInstance *pInstance);

BasicReturnCode MHW_receive(struct MHWInstance *pInstance,
							MACAddress *pAddress,
							uint8_t *pData,
							uint16_t *pDataSize);

uint8_t MHW_transmitterAvailable(   struct Channel *pChannel,
                                    uint32_t pDataSize);

void MHW_transceiverTransmitFunction(   struct Channel *pChannel,
                                        struct Stream *pStream,
                                        unsigned short pDataSize);

#endif	/* MHW_H */
