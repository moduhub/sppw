/* 
 * File:   	MHW.c
 * Authors:	Adan Kvitschal
 *			Jorge Felipe Grybosi
 *
 * Created on 01 de Outubro de 2018
 */

#ifdef PIC32MX664
#include <proc/p32mx664f128h.h>
#include "peripheral/PIC32Timer.h"
#else
#include "peripheral/EFM32Timer.h"
#endif

#include "module/MHW.h"

#include "wireless/SimpleLink.h"

#include "control/Transceiver.h"

#include "mhnet/Datagram.h"
#include "mhnet/HostBase.h"

#include "utils/BasicTypes.h"
#include "core/Stream.h"

#include "crypto/crc.h"

#include <string.h>

/*** Private Prototypes Start ***/

struct MHWDataBuffer * MHW_newRxBuffer(struct MHWInstance *pInstance);

struct MHWSlotData * MHW_newRandomSlot(struct MHWInstance *pInstance);

BasicReturnCode MHW_changeState(struct MHWInstance *pInstance,
								uint8_t pNewState);

BasicReturnCode MHW_checkControlPacket(	uint8_t *pDataBuffer,
										uint16_t pDataSize);

BasicReturnCode MHW_checkDataPacket(uint8_t *pHeader,
                                    uint8_t *pData,
                                    uint16_t pDataSize);

BasicReturnCode MHW_buildControlPacket(	struct MHWInstance *pInstance,
										struct MHWSlotData *pSlotData);

void MHW_planSlotOperations(struct MHWInstance *pInstance);

void MHW_changeSlotOperation(   struct MHWInstance *pInstance,
                                struct MHWSlotData *pSlot,
                                uint8_t pNewOperation,
                                uint8_t pTTL);

void MHW_setSlotRemoteAddress(  struct MHWInstance *pInstance,
                                struct MHWSlotData *pSlot,
                                MACAddress pAddress);

BasicReturnCode MHW_assignDataBuffer(   struct MHWInstance *pInstance,
                                        struct MHWSlotData *pSlotData,
                                        struct MHWDataBuffer *pDataBuffer);

struct MHWDataBuffer * MHW_getAnyUnassignedTxBuffer(struct MHWInstance *pInstance);

struct MHWDataBuffer * MHW_getUnassignedTxBufferByAddress(	struct MHWInstance *pInstance,
															MACAddress pAddress);

MACAddress MHW_getControlPacketSourceAddress(uint8_t *pBuffer);
MACAddress MHW_getControlPacketDestinationAddress(uint8_t *pBuffer);
uint32_t MHW_getControlPacketRxBytes(uint8_t *pBuffer);
uint8_t MHW_getControlPacketTimeToLive(uint8_t *pBuffer);
uint32_t MHW_getControlPacketTimestampFrac(uint8_t *pBuffer);
uint8_t MHW_getControlPacketNextIndex(uint8_t *pBuffer);
uint8_t MHW_getControlPacketNextFrequency(uint8_t *pBuffer);

MACAddress MHW_getDataPacketSourceAddress(uint8_t *pHeader, uint8_t *pData);

BasicReturnCode MHW_transmitDataBuffer( struct MHWInstance *pInstance,
                                        struct MHWDataBuffer *pDataBuffer,
                                        uint8_t tIncludeAddress);

BasicReturnCode MHW_listFreeSlot(   struct MHWInstance *pInstance,
                                    uint8_t pIndex,
                                    uint8_t pFrequency);

struct Channel * MHW_newChannel(struct MHWInstance *pInstance,
                                MACAddress pAddress);

struct Channel * MHW_searchChannel( struct MHWInstance *pInstance,
									MACAddress pAddress);

BasicReturnCode MHW_unlistChannelSlot(  struct Channel *pChannel,
										uint8_t pSlotIndex,
										uint8_t pFrequency);

BasicReturnCode MHW_updateChannel(  struct MHWInstance *pInstance,
                                    MACAddress pRemoteAddress,
									uint8_t pSlotIndex,
									uint8_t pFrequency,
									uint8_t pTTL,
                                    uint32_t pPhase);

BasicReturnCode MHW_deleteChannel(struct MHWInstance *pInstance,
                                    MACAddress pAddress);

BasicReturnCode MHW_flushChannels(struct MHWInstance *pInstance);

BasicReturnCode MHW_countChannelSlots(struct MHWInstance *pInstance,
                                        uint16_t *pSyncedSlots,
                                        uint16_t *pOutOfSyncSlots);

void MHW_runTransceiverTasks(struct MHWInstance *pInstance);

//BasicReturnCode MHW_getPhaseStatistics( struct MHWInstance *pInstance,
//                                        int32_t *pMeanPhase,
//                                        uint32_t *pVariance);

/*** Private Prototypes End	***/

//const uint8_t gLoremIpsum449x[] =   "<00> Lorem ipsum dolor sit amet, consectetur adipiscing elit, se"
//                                    "d doeiusmod tempor incididunt ut labore et dolore magna aliqua. "
//                                    "Ut enim ad minim veniam, quis nostrud exercitation ullamco labor"
//                                    "is nisi ut aliquip ex ea commodo consequat. Duis aute irure dolo"
//                                    "r in reprehenderit in voluptate velit esse cillum dolore eu fugi"
//                                    "at nulla pariatur. Excepteur sint occaecat cupidatat non proiden"
//                                    "t, sunt in culpa qui officia deserunt mollit anim id est laborum"
//                                    ".";

const uint8_t gLoremIpsum128x[] =   "<00> Lorem ipsum dolor sit amet, consectetur adipiscing elit, se"
                                    "d doeiusmod tempor incididunt ut labore et dolore magna aliqua. ";

int32_t gTimestampFracPhase;

uint32_t gCpTxStartTime = 0;
uint32_t gCpTxEndTime = 0;
uint32_t gCpRxEnableTime = 0;
uint32_t gCpRxStartTime = 0;
uint32_t gCpRxEndTime = 0;
uint32_t gCpProcessTime = 0;
uint32_t gDataRxEnableTime = 0;
uint32_t gDataRxDisableTime = 0;
uint32_t gDataRxStartTime = 0;
uint32_t gDataTxStartTime = 0;

uint32_t gFrameTimer = 0;

void MHW_initialize(struct MHWInstance *pInstance, 
                    struct SLRInstance *pSLRInstance,
                    struct Transceiver *pTransceiver) {

	uint8_t tSlotIndex;
	
	pInstance->slr = pSLRInstance;
    
	pInstance->state = MHW_STATE_INITIALIZING;

	pInstance->numIdleSlots = MHW_NUM_TIMESLOTS;

    pInstance->mac.byte[5] = 0x9B;
    pInstance->mac.byte[4] = 0x54;
    pInstance->mac.byte[3] = 0x0C;
    pInstance->mac.byte[2] = 0x00;
    pInstance->mac.byte[1] = 0x00;
    pInstance->mac.byte[0] = 0x01;
    
	//Clear channels
    memset(pInstance->channels, 0, sizeof(struct Channel) * MHW_MAX_CHANNELS);
	memset(pInstance->channelInfo, 0, sizeof(struct MHWChannelInfo) * MHW_MAX_CHANNELS);
	
	//Clear slot data
	memset(pInstance->slot, 0, sizeof(struct MHWSlotData) * MHW_NUM_TIMESLOTS);

	//Clear buffers
	memset(pInstance->txBuffers, 0, sizeof(struct MHWDataBuffer) * MHW_NUM_TX_BUFFERS);
	memset(pInstance->rxBuffers, 0, sizeof(struct MHWDataBuffer) * MHW_NUM_RX_BUFFERS);

	// Initialize all time slots as idle
	for(tSlotIndex=0; tSlotIndex < MHW_NUM_TIMESLOTS; tSlotIndex++ ) {

		pInstance->slot[tSlotIndex].operation = MHW_SLOT_OPERATION_IDLE;
	}

    pInstance->phaseUpdateDizimationCounter = 10 + SYS_rand() % 10;
    
    pInstance->longSniff = 1;
    
    if(pTransceiver != 0) {
        
        pTransceiver->interface = pInstance;
        pTransceiver->transmit = MHW_transceiverTransmitFunction;
        pTransceiver->transmitterAvailable = MHW_transmitterAvailable;

        uint8_t i;

        for(i=0; i<MHW_MAX_CHANNELS; i++) {

            pInstance->channels[i].type = CHANNEL_TYPE_FREE;
            pInstance->channels[i].transceiver = pTransceiver;
            pInstance->channels[i].info = &(pInstance->channelInfo[i]);

            pInstance->channelInfo[i].address = MAC_nullAddress();

        }

    }
    
}


void MHW_run(struct MHWInstance *pInstance) {

	BasicReturnCode tResult;
	uint16_t tDataSize;

    //Test
//    if(pInstance->txBuffers[0].state == MHW_BUFFER_STATE_FREE) {
//    //&& pInstance->txBuffers[1].state == MHW_BUFFER_STATE_FREE) {
//
//        MACAddress tRemoteAddress;
//
//        tRemoteAddress.byte[5] = 0x9B;
//        tRemoteAddress.byte[4] = 0x54;
//        tRemoteAddress.byte[3] = 0x0C;
//        tRemoteAddress.byte[2] = 0x00;
//        tRemoteAddress.byte[1] = 0x00;
//        tRemoteAddress.byte[0] = 0x01;
//
//        MHW_send(pInstance, tRemoteAddress, gLoremIpsum128x, sizeof(gLoremIpsum128x));
//    }
    
	pInstance->timestampInt = TMR_getTimestamp();
	pInstance->timestampFrac = TMR_getTimestampFrac();

    uint32_t tTimestampMs = ((pInstance->timestampFrac>>16) * 1000)>>16;
    
	uint32_t tFrameTimer = tTimestampMs - (pInstance->currentSlotIndex * MHW_TIMESLOT_PERIOD);
    
    gFrameTimer = tFrameTimer;
    
	struct MHWSlotData *tCurrentSlot = &pInstance->slot[pInstance->currentSlotIndex];

	switch(pInstance->state) {
        
		case MHW_STATE_INITIALIZING: {
            
			MHW_changeState(pInstance, MHW_STATE_WAITING_CONTROL_PACKET);
            
        } break;
        
       	case MHW_STATE_NEXT_CHANNEL: {

			//Sleep until the end of current channel

			if(pInstance->slr->state == SLR_STATE_IDLE) {

                uint32_t tNextSlotIndex = tTimestampMs / MHW_TIMESLOT_PERIOD;
                
				if(tNextSlotIndex != pInstance->currentSlotIndex) {

                    pInstance->currentSlotIndex = tNextSlotIndex;
                    
                    tFrameTimer = tTimestampMs - (pInstance->currentSlotIndex * MHW_TIMESLOT_PERIOD);
                    
                    if(tFrameTimer < MHW_CONTROL_PACKET_TIME) {
                        
                        tCurrentSlot = &pInstance->slot[pInstance->currentSlotIndex];
                        
                        switch(tCurrentSlot->operation) {

                            case MHW_SLOT_OPERATION_BROADCAST:
                            case MHW_SLOT_OPERATION_UNICAST_RECEIVE: {

                                SLR_setDataRate(pInstance->slr, SLR_DATARATE_38K);
                                //SLR_setTxPower(pInstance->slr, MHW_MAXIMUM_TX_POWER);
                                //SLR_setChannel(pInstance->slr, tCurrentSlot->frequency);

                                MHW_buildControlPacket(pInstance, tCurrentSlot);

                                MHW_changeState(pInstance, MHW_STATE_TRANSMITTING_CONTROL_PACKET);

                            } break;

                            case MHW_SLOT_OPERATION_SNIFF:
                            case MHW_SLOT_OPERATION_UNICAST_SEND: {

                                SLR_setDataRate(pInstance->slr, SLR_DATARATE_38K);
                                //SLR_setTxPower(pInstance->slr, MHW_MAXIMUM_TX_POWER);
                                //SLR_setChannel(pInstance, tCurrentSlot->frequency);

                                SLR_startReceive(pInstance->slr, pInstance->cpBuffer, 30);

                                gCpRxEnableTime = 0;
                                
                                MHW_changeState(pInstance, MHW_STATE_WAITING_CONTROL_PACKET);


                            } break;

                            case MHW_SLOT_OPERATION_IDLE: {

                                //Use idle slots to process messages
                                MHW_runTransceiverTasks(pInstance);
                                
                                MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

                            } break;
                        }

                        if(pInstance->currentSlotIndex == 0) {

                            //Every frame end, recalculate the actions.
                            MHW_planSlotOperations(pInstance);

                        }
                        
                    }

				}

			} else
			if(pInstance->slr->state == SLR_STATE_RECEIVER_ENABLED
            || pInstance->slr->state == SLR_STATE_RECEIVING) {

                if(SLR_hasWork(pInstance->slr) == 0) {
                    SLR_stopReceive(pInstance->slr, 0);
                }

			}
			
        } break;
        
		case MHW_STATE_TRANSMITTING_CONTROL_PACKET: {
           
            if(tFrameTimer >= MHW_CONTROL_PACKET_TIME/3) {
                
                if(pInstance->slr->state == SLR_STATE_IDLE
                && SLR_hasWork(pInstance->slr) == 0) {
			
                    SLR_sendData(pInstance->slr, pInstance->cpBuffer, 30);
                    
                    gCpTxStartTime = tFrameTimer;
                    
                    pInstance->txStartTimestampFrac = pInstance->timestampFrac;
                    
					switch(tCurrentSlot->operation) {
			    
						case MHW_SLOT_OPERATION_BROADCAST: {
		        
							MHW_changeState(pInstance, MHW_STATE_TRANSMITTING_DATA_PACKET);
		        
						} break;
		    
						case MHW_SLOT_OPERATION_UNICAST_RECEIVE: {
		        
                            if(pInstance->currentRxBuffer == 0) {
                                pInstance->currentRxBuffer = MHW_newRxBuffer(pInstance);
                            }
                            
                            if(pInstance->currentRxBuffer != 0) {
                                
                                MHW_changeState(pInstance, MHW_STATE_WAITING_DATA_PACKET);    
                                
                            } else {
                                
                                MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                                
                            }
                            				
						}break;
                        
                        default: {
                            //Unexpected slot state
                            MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                        } break;
					}
				}
                
			} else             
            if(tFrameTimer >= MHW_CONTROL_PACKET_TIME) {

                //Too late to start transmission
                MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

            }
            
		}break;
        
		case MHW_STATE_WAITING_CONTROL_PACKET: {

			if(pInstance->slr->state == SLR_STATE_RECEIVING) {

                pInstance->rxStartTimestampFrac = pInstance->timestampFrac;

                gCpRxStartTime = tFrameTimer;
                
        		MHW_changeState(pInstance, MHW_STATE_RECEIVING_CONTROL_PACKET);

			} else 
            if(pInstance->slr->state == SLR_STATE_RECEIVER_ENABLED) {

                if(gCpRxEnableTime == 0) {
                    gCpRxEnableTime = tFrameTimer;
                }
                
                if((tFrameTimer >= (8 * MHW_TIMESLOT_PERIOD)/10 && pInstance->longSniff == 1)
                || (tFrameTimer >= MHW_CONTROL_PACKET_TIME && pInstance->longSniff == 0)) {

                    // No data received

                    if(tCurrentSlot->operation == MHW_SLOT_OPERATION_SNIFF) {
                        
                        //Add slot to free list
                        MHW_listFreeSlot(   pInstance,
                                            pInstance->currentSlotIndex,
                                            tCurrentSlot->frequency);

                        //Frequency sweep
//                        if(++tCurrentSlot->attempts == 3) {
//
//                            tCurrentSlot->attempts = 0;
//
//                            //Scan all frequencies
//                            //tCurrentSlot->frequency += 1;
//
//                            if(tCurrentSlot->frequency >= MHW_NUM_FREQUENCIES) {
//                                tCurrentSlot->frequency = 0;
//                            }
//
//                        }

                    } else
                    if(tCurrentSlot->operation == MHW_SLOT_OPERATION_UNICAST_SEND) {

                        #ifdef DEBUG_RADIO_MESSAGES
                        SLR_sendData(pInstance->slr, "utx_miss", 8);
                        #endif

                        if(++tCurrentSlot->attempts == 3) {

                            tCurrentSlot->attempts = 0;

                            //Slot connection lost...

                            // Unlist this slot from its channel
                            struct Channel *tChannel = MHW_searchChannel(pInstance, tCurrentSlot->remoteAddress);

                            if(tChannel != 0) {

                                MHW_unlistChannelSlot(  tChannel,
                                                        pInstance->currentSlotIndex,
                                                        tCurrentSlot->frequency);

                            }
                            
                            MHW_changeSlotOperation(pInstance, tCurrentSlot, MHW_SLOT_OPERATION_IDLE, 0);

                        }

                    }

                    MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

                }

			} else
			if(pInstance->slr->state == SLR_STATE_IDLE) {

                //Fallback, start receive if wasnt started by previous state
                if(SLR_hasWork(pInstance->slr) == 0) {
                    SLR_startReceive(pInstance->slr, pInstance->cpBuffer, 30);
                }

			} else {

				//Invalid radio state or radio busy

			}

		} break;

		case MHW_STATE_RECEIVING_CONTROL_PACKET: {
		
            //Wait until packet end
            if(pInstance->slr->state == SLR_STATE_IDLE) {

                tDataSize = pInstance->slr->packetSize - pInstance->slr->remainingBytes;
                
                if(tDataSize > 0) {

                    tResult = MHW_checkControlPacket(pInstance->cpBuffer, tDataSize);

                    if(tResult == RETURN_SUCCESS) {

                        MHW_changeState(pInstance, MHW_STATE_TREATING_CONTROL_PACKET);

                    } else {

                        #ifdef DEBUG_RADIO_MESSAGES
                        SLR_sendData(pInstance->slr, "cp_er", 5);
                        #endif

                        //Corrupt#endifed packet
                        MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

                    }

                } else {

                    //Empty packet
                    MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

                }
                                   
			}
            
            //Limit reception time
            if(tFrameTimer > MHW_TIMESLOT_PERIOD) {
                
                SLR_stopReceive(pInstance->slr, 0);
                MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                
            }
 
		} break;
        
        case MHW_STATE_TREATING_CONTROL_PACKET: {
        
            if(pInstance->slr->state == SLR_STATE_IDLE) {
                
                gCpRxEndTime = tFrameTimer;
            
                MACAddress tRemoteAddress = MHW_getControlPacketDestinationAddress(pInstance->cpBuffer);

                MACAddress tSourceAddress = MHW_getControlPacketSourceAddress(pInstance->cpBuffer);

                gTimestampFracPhase = pInstance->rxStartTimestampFrac - 0x02000000
                                    - MHW_getControlPacketTimestampFrac(pInstance->cpBuffer);

                if(abs(gTimestampFracPhase) < MHW_MAX_TIME_PHASE) {

                    if(tCurrentSlot->operation == MHW_SLOT_OPERATION_UNICAST_SEND) {

                        uint8_t tControlMacIsBroadcast = MAC_isBroadcast(tSourceAddress);
                        
                        //Check if still allowed to send
                        if(MAC_equals(tSourceAddress, pInstance->mac)
                        || tControlMacIsBroadcast) {

                            //Check if I have something to send
                            if(tCurrentSlot->dataBuffer != 0) {

                                uint32_t tControlPacketTotalRxBytes = MHW_getControlPacketRxBytes(pInstance->cpBuffer);
                                
                                //Check for ack of previous packet on increment of rx bytes
                                if(tCurrentSlot->dataBuffer->state == MHW_BUFFER_STATE_WAITING_ACK) {

                                    if(tControlPacketTotalRxBytes == tCurrentSlot->totalRxBytes + tCurrentSlot->dataBuffer->dataSize) {

                                        tCurrentSlot->totalRxBytes = tControlPacketTotalRxBytes;
                                        
                                        tCurrentSlot->dataBuffer->state = MHW_BUFFER_STATE_FREE;
                                        tCurrentSlot->dataBuffer = 0;
                                        
                                        #ifdef DEBUG_RADIO_MESSAGES
                                        SLR_sendData(pInstance->slr, "free", 4);
                                        #endif

                                        //Assign other packet to send back-to-back
                                        struct MHWDataBuffer *tDataBuffer = MHW_getUnassignedTxBufferByAddress(pInstance, tCurrentSlot->remoteAddress);
                                        
                                        if(tDataBuffer != 0) {

                                            MHW_assignDataBuffer(pInstance, tCurrentSlot, tDataBuffer);
                                            
                                            SLR_setDataRate(pInstance->slr, SLR_DATARATE_250K);
                                        
                                            MHW_transmitDataBuffer(pInstance, tCurrentSlot->dataBuffer, tControlMacIsBroadcast);
                                        
                                            tCurrentSlot->dataBuffer->state = MHW_BUFFER_STATE_WAITING_ACK;
                                    
                                            MHW_changeState(pInstance, MHW_STATE_TRANSMITTING_DATA_PACKET);
                                            
                                        } else {

                                            MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                                            
                                        }
                                        
                                    } else {
                                        
                                        //Retransmit
                                        
                                        SLR_setDataRate(pInstance->slr, SLR_DATARATE_250K);
                                        
                                        MHW_transmitDataBuffer(pInstance, tCurrentSlot->dataBuffer, tControlMacIsBroadcast);
                                        
                                        tCurrentSlot->totalRxBytes = tControlPacketTotalRxBytes;

                                        MHW_changeState(pInstance, MHW_STATE_TRANSMITTING_DATA_PACKET);

                                    }

                                } else
                                if(tCurrentSlot->dataBuffer->state == MHW_BUFFER_STATE_ASSIGNED) {

                                    SLR_setDataRate(pInstance->slr, SLR_DATARATE_250K);
                                    
                                    MHW_transmitDataBuffer(pInstance, tCurrentSlot->dataBuffer, tControlMacIsBroadcast);

                                    tCurrentSlot->dataBuffer->state = MHW_BUFFER_STATE_WAITING_ACK;
                                    tCurrentSlot->totalRxBytes = tControlPacketTotalRxBytes;
                                    
                                    MHW_changeState(pInstance, MHW_STATE_TRANSMITTING_DATA_PACKET);

                                } else {

                                    #ifdef DEBUG_RADIO_MESSAGES
                                    SLR_sendData(pInstance->slr, "er44", 4);
                                    #endif
                                    
                                    //Buffer assigned to channel, but in an invalid state
                                    tCurrentSlot->dataBuffer = 0;

                                    MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                                    
                                }

                            } else {

                                #ifdef DEBUG_RADIO_MESSAGES
                                SLR_sendData(pInstance->slr, "nodat", 5);
                                #endif  
                                
                                //No data to send to this unicast address
                                                                
                                MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                                
                            }

                        } else {

                            //Slot has been closed on a different source
                            struct Channel *tChannel = MHW_searchChannel(pInstance, tRemoteAddress);

                            if(tChannel != 0) {

                                MHW_unlistChannelSlot(  tChannel,
                                                        pInstance->currentSlotIndex,
                                                        tCurrentSlot->frequency);

                            }

                            MHW_changeSlotOperation(pInstance, tCurrentSlot, MHW_SLOT_OPERATION_IDLE, 0);
                            
                            MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                            
                        }

                    } else
                    if(tCurrentSlot->operation == MHW_SLOT_OPERATION_SNIFF) {

                        //Perform some basic check on the control packet mac
                        if(MAC_isBroadcast(tRemoteAddress) == 0
                        && MAC_isNull(tRemoteAddress) == 0) {

                            //Store only open channels
                            if(MAC_isBroadcast(tSourceAddress)) {
                                
                                MHW_updateChannel(  pInstance,
                                                    tRemoteAddress,
                                                    pInstance->currentSlotIndex,
                                                    tCurrentSlot->frequency,
                                                    MHW_getControlPacketTimeToLive(pInstance->cpBuffer),
                                                    gTimestampFracPhase);
                                
                            } else {
                            
                                //Track
                                uint8_t tNextIndex = MHW_getControlPacketNextIndex(pInstance->cpBuffer);
                                uint8_t tNextFrequency = MHW_getControlPacketNextFrequency(pInstance->cpBuffer);

                                if(tNextIndex < 20 && tNextFrequency < 32) {

                                    struct MHWSlotData *tNextSlot = &(pInstance->slot[tNextIndex]);

                                    if(tNextSlot->operation == MHW_SLOT_OPERATION_IDLE) {
                                        MHW_changeSlotOperation(pInstance, tNextSlot, MHW_SLOT_OPERATION_SNIFF, 3);
                                        tNextSlot->frequency = tNextFrequency;
                                    } else
                                    if(tNextSlot->operation == MHW_SLOT_OPERATION_SNIFF) {
                                        tNextSlot->frequency = tNextFrequency;
                                    }

                                }
                                
                            }
                            
                        }

                        MHW_changeSlotOperation(pInstance, tCurrentSlot, MHW_SLOT_OPERATION_IDLE, 0);
                        
                        MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

                    } else {

                        //Unexpected state?
                        MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

                    }

                } else {

                    //uint32_t cpFrac = TMR_getTimestampFrac();//MHW_getControlPacketTimestampFrac(pInstance->cpBuffer);
                    //cpFrac -= gTimestampFracPhase;
                    //TMR_setTimestampFrac(cpFrac);
                    //SLR_sendData(pInstance->slr, "get_sync", 8);
                    
                    //This packet is out of phase
                    MHW_updateChannel(  pInstance,
                                        tRemoteAddress,
                                        pInstance->currentSlotIndex,
                                        tCurrentSlot->frequency,
                                        MHW_getControlPacketTimeToLive(pInstance->cpBuffer),
                                        gTimestampFracPhase);

                    MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                    
                }

            } else 
            if(pInstance->slr->state == SLR_STATE_RECEIVING) {

                if(SLR_hasWork(pInstance->slr) == 0) {
                    SLR_stopReceive(pInstance->slr, 0);
                }

            } else {

                //Radio in unexpected state or busy

            }

        } break;

        case MHW_STATE_TRANSMITTING_DATA_PACKET: {

            if(pInstance->slr->state == SLR_STATE_IDLE) {
                
                if(SLR_hasWork(pInstance->slr) == 0) {

                    MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                    
                }

            } else
            if(pInstance->slr->state == SLR_STATE_TRANSMITTING) {
                
                if(gDataTxStartTime == 0) {
                    gDataTxStartTime = tFrameTimer;
                }

            }

        } break;

        case MHW_STATE_WAITING_DATA_PACKET: {

            if(tFrameTimer < MHW_CONTROL_PACKET_TIME + MHW_DATA_PACKET_MAX_DELAY) {

                if(pInstance->slr->state == SLR_STATE_RECEIVER_ENABLED) {
                    
                    if(gDataRxEnableTime == 0) {
                        gDataRxEnableTime = tFrameTimer;
                    }
                    
                }else
                if(pInstance->slr->state == SLR_STATE_RECEIVING) {

                    gDataRxEnableTime = tFrameTimer;
                    
                    tCurrentSlot->lastRSSI = pInstance->slr->rssi;
                    
                    MHW_changeState(pInstance, MHW_STATE_RECEIVING_DATA_PACKET);

                } else 
                if(SLR_hasWork(pInstance->slr) == 0) {

                    //The radio comes from the previous state still
                    // transmitting the control packet.
                    
                    if(pInstance->slr->state == SLR_STATE_IDLE) {

                        if(pInstance->currentRxBuffer != 0) {
                        
                            pInstance->messageSizeReceived = 0;
                            
                            SLR_setDataRate(pInstance->slr, SLR_DATARATE_250K);
                        
                            SLR_startReceive(pInstance->slr, pInstance->currentRxBuffer->data, MHW_MTU_SIZE);
                            
                            gCpTxEndTime = tFrameTimer;
                            gDataRxEnableTime = 0;
                            gDataRxStartTime = 0;
                            
                        } else {
                            
                            //Should never happen, reaching here without a buffer allocated
                            MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                            
                        }
                        
                    }
                    
                }

            } else {

                gDataRxDisableTime = tFrameTimer;
                
                //No data received
                MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

            }

        } break;

        case MHW_STATE_RECEIVING_DATA_PACKET: {

            /*if(pInstance->slr->state == SLR_STATE_RECEIVING) {
                
                if(pInstance->messageSizeReceived == 0) {
                    
                    //Peek packet size and set rx length
                    uint16_t tNumRxBytes = SLR_getNumReceivedBytes(pInstance->slr);

                    if(tNumRxBytes > 2) {

                        Word tMessageSize;

                        tMessageSize.byte[1] = pInstance->currentRxBuffer->data[0];
                        tMessageSize.byte[0] = pInstance->currentRxBuffer->data[1];
                        
                        if(tMessageSize.word > 4 && tMessageSize.word <= MHW_MTU_SIZE) {
                        
                            SLR_stopReceive(pInstance->slr, tMessageSize.word);

                            pInstance->messageSizeReceived = 1;
                            
                        } else {
                            
                            //Invalid message size
                            
                            SLR_stopReceive(pInstance->slr, 0);
                            
                            MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);
                            
                        }

                    }
                    
                }
                
            } else*/
            if(pInstance->slr->state == SLR_STATE_IDLE) {
                            
                Word tMessageSize;

                tMessageSize.byte[1] = pInstance->currentRxBuffer->data[0];
                tMessageSize.byte[0] = pInstance->currentRxBuffer->data[1];
                
                pInstance->currentRxBuffer->dataSize = tMessageSize.word;
                
                //pInstance->currentRxBuffer->dataSize = SLR_getNumReceivedBytes(pInstance->slr);
                
                //Radio has concluded reception
                if(pInstance->currentRxBuffer->dataSize > 6
                && pInstance->currentRxBuffer->dataSize < MHW_MTU_SIZE) {

                    tResult = MHW_checkDataPacket(  pInstance->cpBuffer,
                                                    pInstance->currentRxBuffer->data,
                                                    pInstance->currentRxBuffer->dataSize);

                    if(tResult == RETURN_SUCCESS) {
                        
                        if(MAC_isBroadcast(tCurrentSlot->remoteAddress)) {

                            //Close slot on this host, if everything is correct
                            MHW_setSlotRemoteAddress(   pInstance,
                                                        tCurrentSlot,
                                                        MHW_getDataPacketSourceAddress(pInstance->cpBuffer, pInstance->currentRxBuffer->data));

                            pInstance->currentRxBuffer->dataOffset = 8;

                        } else {

                            pInstance->currentRxBuffer->dataOffset = 2;

                        }

                        //Calculate Rx bytes for ack signal
                        tCurrentSlot->totalRxBytes += pInstance->currentRxBuffer->dataSize
                                                    - pInstance->currentRxBuffer->dataOffset - 4;

                        //tCurrentSlot->dataBuffer = tNewRxBuffer;

                        pInstance->currentRxBuffer->address = tCurrentSlot->remoteAddress;
                        pInstance->currentRxBuffer->state = MHW_BUFFER_STATE_WAITING_READ;
                        
                        pInstance->currentRxBuffer = 0;
                        
                        #ifdef DEBUG_RADIO_MESSAGES
                        SLR_sendData(pInstance->slr, "rx_ok", 5);
                        #endif

                        MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

                    } else {

                        #ifdef DEBUG_RADIO_MESSAGES
                        SLR_sendData(pInstance->slr, "crc_err", 7);
                        #endif

                        MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

                    }                            

                } else {

                    #ifdef DEBUG_RADIO_MESSAGES
                    SLR_sendData(pInstance->slr, "nulpac", 6);
                    #endif

                    MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

                }

            }
            
            if(tFrameTimer > MHW_TIMESLOT_PERIOD) {

                #ifdef DEBUG_RADIO_MESSAGES
                SLR_sendData(pInstance->slr, "er1", 3);
                #endif

                SLR_stopReceive(pInstance->slr, 0);

                //Timeout while receiving packet, strange situation...
                MHW_changeState(pInstance, MHW_STATE_NEXT_CHANNEL);

            }

		} break;
                
        default: {
            
            pInstance->state = 0x01;
            
        } break;
	}
}


struct MHWSlotData * MHW_newRandomSlot(struct MHWInstance *pInstance) {

	struct MHWSlotData *tNewSlot = 0;

	uint8_t tSlotIndex;

	if(pInstance->numIdleSlots > 0) {

		//Choose one slot at random
		tSlotIndex = SYS_rand() % MHW_NUM_TIMESLOTS;

		do {

			// Check if slot is idle
			if(pInstance->slot[tSlotIndex].operation == MHW_SLOT_OPERATION_IDLE) {

				tNewSlot = &pInstance->slot[tSlotIndex];

			} else {

				//Move until an idle slot is found
				if(++tSlotIndex == MHW_NUM_TIMESLOTS) {
					tSlotIndex=0;
				}

			}

		} while(tNewSlot == 0);

		tNewSlot->attempts = 0;
		tNewSlot->frequency = tSlotIndex = SYS_rand() % MHW_NUM_FREQUENCIES;
		tNewSlot->remoteAddress = MAC_nullAddress();
        tNewSlot->totalRxBytes = 0;
        tNewSlot->dataBuffer = 0;

	}

	return tNewSlot;
}


struct Channel * MHW_newChannel(struct MHWInstance *pInstance,
                                MACAddress pAddress) {
    
    uint8_t i;
    struct Channel *tChannel = 0;
    
    uint32_t tOldestTimestamp = 0;
    
    for(i=0; i<MHW_MAX_CHANNELS; i++) {
        
        if(pInstance->channels[i].type == CHANNEL_TYPE_FREE) {
                
            tChannel = &(pInstance->channels[i]);
            tOldestTimestamp = 0;

            break;            
            
        } else {
            
            if(pInstance->channels[i].lastTimestamp < tOldestTimestamp
            || tOldestTimestamp == 0) {
                
                tChannel = &(pInstance->channels[i]);
                tOldestTimestamp = pInstance->channels[i].lastTimestamp;
                
            }
            
        }

    }
    
    if(tChannel != 0) {
        
        if(tOldestTimestamp != 0) {
            HostBase_onChannelDestroy(tChannel);
        }
        
        struct MHWChannelInfo * tChannelInfo = (struct MHWChannelInfo *) tChannel->info;
        
        tChannelInfo->address = pAddress;
        
        tChannel->type = CHANNEL_TYPE_MHW;
        
    }
    
    return tChannel;
}


struct Channel * MHW_getChannelFromAddress( struct MHWInstance *pInstance,
                                            MACAddress pAddress) {

    uint8_t i;
    struct Channel *tChannel = 0;
    
    for(i=0; i<MHW_MAX_CHANNELS; i++) {
        
        struct MHWChannelInfo *tChannelInfo = (struct MHWChannelInfo *) pInstance->channels[i].info;
        
        if(MAC_equals(tChannelInfo->address, pAddress)) {
            
            tChannel = &(pInstance->channels[i]);
            
            break;
            
        } 

    }
    
    return tChannel;
}


void MHW_runTransceiverTasks(struct MHWInstance *pInstance) {
    
    uint8_t i;
    
    for(i=0; i<MHW_NUM_RX_BUFFERS; i++) {

        struct MHWDataBuffer *iBuffer = &pInstance->rxBuffers[i];

        if(iBuffer->state == MHW_BUFFER_STATE_WAITING_READ) {
            
            struct Stream tBufferStream;
            
            //Create buffer stream and pass MHW buffer data to it
            SYS_configureBufferStream(&tBufferStream);

            uint8_t *tDataPointer = iBuffer->data + iBuffer->dataOffset;
            uint16_t tDataSize = iBuffer->dataSize - iBuffer->dataOffset - 4;

            tBufferStream.writeBuffer(tDataPointer, tDataSize);
            
            // Search or create new assigned channel
            struct Channel *tChannel = MHW_getChannelFromAddress(pInstance, iBuffer->address);
            
            if(tChannel == 0) {
                
                //Create new MHW channel
                tChannel = MHW_newChannel(pInstance, iBuffer->address);
                
                HostBase_onChannelCreate(tChannel);
                
            }
            
            if(tChannel != 0) {
                tChannel->lastTimestamp = TMR_getCurrentTimestamp();
            }
            
            struct DatagramBuffer *tNewIncommingDatagram = Datagram_newIncommingDatagram(tDataSize);

            if(tNewIncommingDatagram != 0) {

				BasicReturnCode tResult = Datagram_parse(&tBufferStream, tChannel, 0, tNewIncommingDatagram);

				if(tResult == RETURN_SUCCESS) {

					tNewIncommingDatagram->state = MHN_DATAGRAM_STATE_RECEIVED;

				} else {

					Datagram_destroy(tNewIncommingDatagram);

				}

            }

            //Free the buffer
            iBuffer->state = MHW_BUFFER_STATE_FREE;
            iBuffer->dataSize = 0;
            iBuffer->dataOffset = 0;

        }

    }
        
}


uint8_t MHW_transmitterAvailable(   struct Channel *pChannel,
                                    uint32_t pDataSize) {

    uint8_t tTransmitterAvailable = 0;
    uint8_t i;

    struct Transceiver *tTransceiver = (struct Transceiver *) pChannel->transceiver;
    struct MHWInstance *tMHWInstance = (struct MHWInstance *) tTransceiver->interface;
    
    if(pDataSize <= MHW_MTU_SIZE) {
    
        for(i=0; i<MHW_NUM_TX_BUFFERS; i++) {

            if(tMHWInstance->txBuffers[i].state == MHW_BUFFER_STATE_FREE) {
                
                tTransmitterAvailable = 1;
                break;

            }

        }
        
    }
    
    return tTransmitterAvailable;
}


void MHW_transceiverTransmitFunction(   struct Channel *pChannel,
                                        struct Stream *pStream,
                                        unsigned short pDataSize) {
    
    uint8_t i;
    
    struct MHWDataBuffer *tBuffer = 0;
    
    if(pChannel != 0
    && pStream != 0
    && pDataSize > 0
    && pDataSize < MHW_MTU_SIZE) {
    
        struct Transceiver *tTransceiver = (struct Transceiver *) pChannel->transceiver;
        
        struct MHWInstance *tInstance = (struct MHWInstance *) tTransceiver->interface;
        
        for(i=0; i< MHW_NUM_TX_BUFFERS; i++) {

			if(tInstance->txBuffers[i].state == MHW_BUFFER_STATE_FREE) {

				tBuffer = &(tInstance->txBuffers[i]);

				break;
			}
		}

		if(tBuffer != 0) {

            struct MHWChannelInfo *tChannelInfo = pChannel->info;
            
            pStream->readBuffer(tBuffer->data, pDataSize);

			tBuffer->dataSize = pDataSize;

			tBuffer->address = tChannelInfo->address;

            tBuffer->timeout = 0;
            
            tBuffer->state = MHW_BUFFER_STATE_WAITING_ASSIGNMENT;
            
            //Attempt to assign buffer immediately
            for(i=0; i<MHW_NUM_TIMESLOTS; i++) {
                
                struct MHWSlotData *iSlot = &(tInstance->slot[i]);
                
                if(iSlot->operation == MHW_SLOT_OPERATION_UNICAST_SEND
                && iSlot->dataBuffer == 0
                && MAC_equals(tBuffer->address, iSlot->remoteAddress)) {
                    
                    MHW_assignDataBuffer(tInstance, iSlot, tBuffer);
                    break;
                    
                }
            }            
            
		}
        
    }
    
}


uint8_t MHW_getNumberOfReceivedPackets(struct MHWInstance *pInstance) {

	uint8_t tNumRxPackets = 0;
	uint8_t i;

	for(i=0; i<MHW_NUM_RX_BUFFERS;i++) {

		if(pInstance->rxBuffers[i].state == MHW_BUFFER_STATE_WAITING_READ) {
			tNumRxPackets++;
		}

	}

	return tNumRxPackets;
}


BasicReturnCode MHW_receive(struct MHWInstance *pInstance,
							MACAddress *pAddress,
							uint8_t *pData,
							uint16_t *pDataSize) {

	BasicReturnCode tReturnCode = RETURN_ERROR;
	uint8_t i;

	if(pDataSize != 0
    && pAddress != 0) {

		for(i=0; i<MHW_NUM_RX_BUFFERS; i++) {

            struct MHWDataBuffer *iBuffer = &pInstance->rxBuffers[i];
            
			if(iBuffer->state == MHW_BUFFER_STATE_WAITING_READ) {

				if(iBuffer->dataSize > *pDataSize) {

					tReturnCode = RETURN_ERROR_BUFFER_TOO_SMALL;
					break;

				} else {

					*pDataSize = iBuffer->dataSize - iBuffer->dataOffset - 4;
                    *pAddress = iBuffer->address;
                    
					memcpy(pData, iBuffer->data + iBuffer->dataOffset, *pDataSize);

					iBuffer->state = MHW_BUFFER_STATE_FREE;
					iBuffer->dataSize = 0;
                    iBuffer->dataOffset = 0;

					tReturnCode = RETURN_SUCCESS;

				}

			}

		}

	} else {

		tReturnCode = RETURN_ERROR_INVALID_PARAMETERS;

	}

	return tReturnCode;
}


/*
 *	/brief	This function run once every second, to process each slot as a state machine.
 */
//uint8_t gGambi11 = 0;

void MHW_planSlotOperations(struct MHWInstance *pInstance) {

	uint8_t tSlotIndex;
	uint8_t i, j;

    uint16_t tSyncedSlots, tOutOfSyncSlots;
    
//    if(pInstance->phaseUpdateDizimationCounter > 0) {
//        
//        pInstance->phaseUpdateDizimationCounter--;
//        
//    } else {
//
//        pInstance->phaseUpdateDizimationCounter = 10 + SYS_rand() % 10;
        
        //Sync control
        MHW_countChannelSlots(pInstance, &tSyncedSlots, &tOutOfSyncSlots);
        
        if(tSyncedSlots > 0) {
             pInstance->longSniff = 0;
        } else {
             pInstance->longSniff = 1;
        }
        
        if(tOutOfSyncSlots > tSyncedSlots) {

            uint32_t tPhase = 0;
            
            //Assume the phase of a randomly selected out of sync channel
            int32_t tRandomChannelIndex = SYS_rand() % MHW_MAX_CHANNELS;
            
            for(i=0; i<MHW_MAX_CHANNELS; i++) {

                struct MHWChannelInfo *tChannelInfo = pInstance->channels[tRandomChannelIndex].info;

                for(j=0; j<MHW_MAX_CHANNEL_SLOTS; j++) {
                    if(tChannelInfo->slots[j].timeToLive > 0
                    && tChannelInfo->slots[j].phase > MHW_MAX_TIME_PHASE) {
                        tPhase = tChannelInfo->slots[j].phase;
                        break;
                    }
                }
                
                if(tPhase != 0) {
                    break;
                }
                tRandomChannelIndex = (++tRandomChannelIndex)% MHW_MAX_CHANNELS;
            }
            
            uint32_t tCurrentFrac = TMR_getTimestampFrac();
            
            TMR_setTimestampFrac(tCurrentFrac - tPhase);
                
            MHW_flushChannels(pInstance);
                
        } else {

            //Perform small phase correction to a random synced channel
            uint32_t tPhase = 0;
            
            int32_t tRandomChannelIndex = SYS_rand() % MHW_MAX_CHANNELS;
            
            for(i=0; i<MHW_MAX_CHANNELS; i++) {

                struct MHWChannelInfo *tChannelInfo = pInstance->channels[tRandomChannelIndex].info;

                for(j=0; j<MHW_MAX_CHANNEL_SLOTS; j++) {
                    if(tChannelInfo->slots[j].timeToLive > 0
                    && tChannelInfo->slots[j].phase <= MHW_MAX_TIME_PHASE) {
                        tPhase = tChannelInfo->slots[j].phase;
                        break;
                    }
                }
                
                if(tPhase != 0) {
                    break;
                }
                
                tRandomChannelIndex = (++tRandomChannelIndex)% MHW_MAX_CHANNELS;
            }
            
            uint32_t tCurrentFrac = TMR_getTimestampFrac();
            
            TMR_setTimestampFrac(tCurrentFrac - tPhase);
            
            //Fix all phases
            for(i=0; i<MHW_MAX_CHANNELS; i++) {

                struct MHWChannelInfo *tChannelInfo = pInstance->channels[i].info;

                for(j=0; j<MHW_MAX_CHANNEL_SLOTS; j++) {
                    if(tChannelInfo->slots[j].timeToLive > 0) {
                        tChannelInfo->slots[j].phase -= tPhase;
                    }
                }
            }
                
        }
        
//    }
    
    //Update free slots times
    for(i=0; i<MHW_FREE_SLOT_LIST_SIZE; i++) {
        if(pInstance->freeSlots[i].timeSinceLastUpdate < 30) {
            pInstance->freeSlots[i].timeSinceLastUpdate++;
        }
    }
        
    //Update channels TTLs
    for(i=0; i<MHW_MAX_CHANNELS; i++) {
        
        struct Channel * tChannel = &(pInstance->channels[i]);
        struct MHWChannelInfo *tChannelInfo = tChannel->info;

        if(tChannel->type != CHANNEL_TYPE_FREE) {
        
            uint8_t tChannelNumSlots = 0;

            for(j=0; j<MHW_MAX_CHANNEL_SLOTS; j++) {

                if(tChannelInfo->slots[j].timeToLive > 0) {

                    if(--tChannelInfo->slots[j].timeToLive > 0) {

                        tChannelNumSlots++;

                    }

                }
            }

            if(tChannelNumSlots == 0) {

                //Host disappeared!
                MHW_deleteChannel(pInstance, tChannelInfo->address);

            }
            
        }
                
    }
    
	if(pInstance->numOpenRxSlots < MHW_NUM_OPEN_SLOTS
    && pInstance->numIdleSlots > 0) {

        //Select a slot from the free list
        for(i=0; i<MHW_FREE_SLOT_LIST_SIZE; i++) {
            
            if(pInstance->freeSlots[i].timeSinceLastUpdate < 5) {
            
                uint8_t tNewOpenSlotIndex = pInstance->freeSlots[i].index;
                
                struct MHWSlotData *tNewOpenSlot = &(pInstance->slot[tNewOpenSlotIndex]);
                
                if(tNewOpenSlot->operation == MHW_SLOT_OPERATION_IDLE
                || tNewOpenSlot->operation == MHW_SLOT_OPERATION_SNIFF) {
                    
                    MHW_changeSlotOperation(pInstance, tNewOpenSlot, MHW_SLOT_OPERATION_UNICAST_RECEIVE, 50 + SYS_rand() % 20);
                    
                    break;
                    
                }
                
            }
            
        }
        
    }

	//Control slot allocation
	while(pInstance->numSniffSlots < MHW_NUM_SNIFF_SLOTS
    && pInstance->numIdleSlots > 0) {

		struct MHWSlotData *tNewSniffSlot = MHW_newRandomSlot(pInstance);

		if(tNewSniffSlot != 0) {

            MHW_changeSlotOperation(pInstance, tNewSniffSlot, MHW_SLOT_OPERATION_SNIFF, 3 + SYS_rand() % 2);

		}

	}
        
	//Control slots as state machines
	for(i=0; i<MHW_NUM_TIMESLOTS; i++) {

		struct MHWSlotData *tSlot = &(pInstance->slot[i]);

        if(tSlot->timeToLive > 0) {
            
            if(--tSlot->timeToLive == 0) {

                MHW_changeSlotOperation(pInstance, tSlot, MHW_SLOT_OPERATION_IDLE, 0);

            } else {

                switch(tSlot->operation) {

                    case MHW_SLOT_OPERATION_SNIFF: {

                        //Use data from sniffing process

                    } break;

                    case MHW_SLOT_OPERATION_UNICAST_SEND: {

                        if(tSlot->dataBuffer == 0) {
                        
                            struct MHWDataBuffer *tDataBuffer = MHW_getUnassignedTxBufferByAddress(pInstance, tSlot->remoteAddress);

                            if(tDataBuffer != 0) {

                                MHW_assignDataBuffer(pInstance, tSlot, tDataBuffer);

                            }
                            
                        }

                    } break;

                    default: {
                    } break;

                }
            }
        }
	}

    //Update slots chaining
    struct MHWSlotData *tPreviousOpenSlot = 0;

    pInstance->firstOpenSlotIndex = 255;
    
	for(i=0; i<MHW_NUM_TIMESLOTS; i++) {

		struct MHWSlotData *tSlot = &(pInstance->slot[i]);

        if(tSlot->timeToLive > 5) {
            
            if(tSlot->operation == MHW_SLOT_OPERATION_UNICAST_RECEIVE
            && MAC_isBroadcast(tSlot->remoteAddress)) {
                
                if(tPreviousOpenSlot == 0) {
                    pInstance->firstOpenSlotIndex = i;
                } else {
                    tPreviousOpenSlot->nextSlotIndex = i;                    
                }
                
                tPreviousOpenSlot = tSlot;
            }
            
        }
        
    }
    
    //Last link
    if(tPreviousOpenSlot != 0) {
        tPreviousOpenSlot->nextSlotIndex = pInstance->firstOpenSlotIndex;
    }

    // Assign all closed slots to point to the first open one
    for(i=0; i<MHW_NUM_TIMESLOTS; i++) {

		struct MHWSlotData *tSlot = &(pInstance->slot[i]);

        if(tSlot->operation == MHW_SLOT_OPERATION_UNICAST_RECEIVE
        && MAC_isBroadcast(tSlot->remoteAddress) == 0) {
            tSlot->nextSlotIndex = pInstance->firstOpenSlotIndex;
        }
        
    }
    
	//If any packet remains unassigned, the host must open more connections
	struct MHWDataBuffer * tUnassignedTxBuffer = MHW_getAnyUnassignedTxBuffer(pInstance);

	if(tUnassignedTxBuffer != 0) {

		//Search for an open unicast address
        struct Channel *tChannel = MHW_searchChannel(pInstance, tUnassignedTxBuffer->address);

        if(tChannel != 0) {

            struct MHWChannelInfo * tChannelInfo = (struct MHWChannelInfo *) tChannel->info;
            
            //Check if one of the known slots is good
            for(i=0; i<MHW_MAX_CHANNEL_SLOTS; i++) {
                
                if(tChannelInfo->slots[i].timeToLive > 5
                && abs(tChannelInfo->slots[i].phase) < MHW_MAX_TIME_PHASE) {
                
                    struct MHWSlotData *tSlotData = &pInstance->slot[tChannelInfo->slots[i].index];
                
                    if(tSlotData->operation == MHW_SLOT_OPERATION_IDLE
                    || tSlotData->operation == MHW_SLOT_OPERATION_SNIFF) {
                        
                        //Open a new unicast transmit address
                        MHW_changeSlotOperation(pInstance, tSlotData,
                                                MHW_SLOT_OPERATION_UNICAST_SEND,
                                                tChannelInfo->slots[i].timeToLive);
                        
                        // Assign to known host slot and frequency, also to pending data buffer
                        MHW_setSlotRemoteAddress(pInstance, tSlotData, tChannelInfo->address);
                        tSlotData->frequency = tChannelInfo->slots[i].frequency;
                        
                        MHW_assignDataBuffer(pInstance, tSlotData, tUnassignedTxBuffer);
                        
                        break;
                    }
                    
                }
                
            }
            
        }
        
		//tUnassignedTxBuffer = MHW_getAnyUnassignedTxBuffer(pInstance);
	}

}


BasicReturnCode MHW_transmitDataBuffer( struct MHWInstance *pInstance,
                                        struct MHWDataBuffer *pDataBuffer,
                                        uint8_t tIncludeAddress) {
    
    BasicReturnCode tReturnCode = RETURN_SUCCESS;
    
    //Recalculate tx data with current control packet
    DoubleWord tCRC;
    Word tDataSize;
    
    tCRC.dword = CRC_initCRC32();
    tCRC.dword = CRC_updateCRC32(tCRC.dword, pInstance->cpBuffer, 30);
    
    if(tIncludeAddress) {

        tDataSize.word = pDataBuffer->dataSize + 12;
        
        pDataBuffer->header[0] = tDataSize.byte[1];
        pDataBuffer->header[1] = tDataSize.byte[0];
        
        pDataBuffer->header[2] = pInstance->mac.byte[5];
        pDataBuffer->header[3] = pInstance->mac.byte[4];
        pDataBuffer->header[4] = pInstance->mac.byte[3];
        pDataBuffer->header[5] = pInstance->mac.byte[2];
        pDataBuffer->header[6] = pInstance->mac.byte[1];
        pDataBuffer->header[7] = pInstance->mac.byte[0];
        
        tCRC.dword = CRC_updateCRC32(tCRC.dword, pDataBuffer->header, pDataBuffer->dataSize + 8);
        tCRC.dword = CRC_finalCRC32(tCRC.dword);

        pDataBuffer->data[pDataBuffer->dataSize + 0] = tCRC.byte[3];
        pDataBuffer->data[pDataBuffer->dataSize + 1] = tCRC.byte[2];
        pDataBuffer->data[pDataBuffer->dataSize + 2] = tCRC.byte[1];
        pDataBuffer->data[pDataBuffer->dataSize + 3] = tCRC.byte[0];
        
        //SYS_delay(10);
        
        SLR_sendData(	pInstance->slr,
                        pDataBuffer->header,
                        tDataSize.word);
        
    } else {
        
        tDataSize.word = pDataBuffer->dataSize + 6;
        
        pDataBuffer->header[6] = tDataSize.byte[1];
        pDataBuffer->header[7] = tDataSize.byte[0];       
        
        tCRC.dword = CRC_updateCRC32(tCRC.dword, pDataBuffer->header + 6, pDataBuffer->dataSize + 2);
        tCRC.dword = CRC_finalCRC32(tCRC.dword);

        pDataBuffer->data[pDataBuffer->dataSize + 0] = tCRC.byte[3];
        pDataBuffer->data[pDataBuffer->dataSize + 1] = tCRC.byte[2];
        pDataBuffer->data[pDataBuffer->dataSize + 2] = tCRC.byte[1];
        pDataBuffer->data[pDataBuffer->dataSize + 3] = tCRC.byte[0];
        
        //SYS_delay(10);
        
        SLR_sendData(	pInstance->slr,
                        pDataBuffer->header + 6,
                        tDataSize.word);
        
    }
     
    return tReturnCode;    
}


void MHW_changeSlotOperation(   struct MHWInstance *pInstance,
                                struct MHWSlotData *pSlot,
                                uint8_t pNewOperation,
                                uint8_t pTTL) {
    
    //Ending operation
    switch(pSlot->operation) {
        case MHW_SLOT_OPERATION_IDLE: {
            pInstance->numIdleSlots--;
        } break;
        
        case MHW_SLOT_OPERATION_SNIFF: {
            pInstance->numSniffSlots--;
        } break;
        
        case MHW_SLOT_OPERATION_UNICAST_RECEIVE: {
            if(MAC_isBroadcast(pSlot->remoteAddress)) {
                pInstance->numOpenRxSlots--;
            } else {
                pInstance->numUnicastRxSlots--;
            }
        } break;

        case MHW_SLOT_OPERATION_UNICAST_SEND: {
            
            pInstance->numUnicastTxSlots--;
            
            if(pSlot->dataBuffer != 0) {
                pSlot->dataBuffer->state = MHW_BUFFER_STATE_WAITING_ASSIGNMENT;
                pSlot->dataBuffer = 0;
            }
            
        } break;
    }
    
    pSlot->operation = pNewOperation;
    pSlot->timeToLive = pTTL;
    
    pSlot->lastRSSI = 0;
    
    //Starting operation
    switch(pNewOperation) {
        case MHW_SLOT_OPERATION_IDLE: {
            pInstance->numIdleSlots++;
        } break;
        
        case MHW_SLOT_OPERATION_SNIFF: {
            pInstance->numSniffSlots++;
        } break;
        
        case MHW_SLOT_OPERATION_UNICAST_RECEIVE: {
            pInstance->numOpenRxSlots++;
            pSlot->remoteAddress = MAC_broadcastAddress();
        } break;

        case MHW_SLOT_OPERATION_UNICAST_SEND: {
            pInstance->numUnicastTxSlots++;
        } break;
        
    }
        
}


void MHW_setSlotRemoteAddress(  struct MHWInstance *pInstance,
                                struct MHWSlotData *pSlot,
                                MACAddress pAddress) {
    
    if(pSlot->operation == MHW_SLOT_OPERATION_UNICAST_RECEIVE) {
   
        if(MAC_isBroadcast(pSlot->remoteAddress) != 0
        && MAC_isBroadcast(pAddress) == 0) {
            pInstance->numOpenRxSlots--;
            pInstance->numUnicastRxSlots++;
        }
        
        if(MAC_isBroadcast(pSlot->remoteAddress) == 0
        && MAC_isBroadcast(pAddress) != 0) {
            pInstance->numOpenRxSlots++;
            pInstance->numUnicastRxSlots--;
        }
        
    }
    
    pSlot->remoteAddress = pAddress;

}


BasicReturnCode MHW_assignDataBuffer(   struct MHWInstance *pInstance,
                                        struct MHWSlotData *pSlotData,
                                        struct MHWDataBuffer *pDataBuffer) {
    
    BasicReturnCode tReturnCode = RETURN_ERROR_INVALID_PARAMETERS;
    
    if(pDataBuffer->state == MHW_BUFFER_STATE_WAITING_ASSIGNMENT
    && pSlotData->operation == MHW_SLOT_OPERATION_UNICAST_SEND) {
        
        //Free previous assigned slot if any
        if(pSlotData->dataBuffer != 0) {
            pSlotData->dataBuffer->state = MHW_BUFFER_STATE_WAITING_ASSIGNMENT;
        }
        
        pSlotData->dataBuffer = pDataBuffer;
        
        pDataBuffer->state = MHW_BUFFER_STATE_ASSIGNED;
        
        tReturnCode = RETURN_SUCCESS;
        
    }
    
    return tReturnCode;
}


struct MHWDataBuffer * MHW_getUnassignedTxBufferByAddress(	struct MHWInstance *pInstance,
															MACAddress pAddress) {

	struct MHWDataBuffer *tDataBuffer = 0;
	uint8_t i;

	for(i=0; i<MHW_NUM_TX_BUFFERS; i++) {

		if(pInstance->txBuffers[i].state == MHW_BUFFER_STATE_WAITING_ASSIGNMENT
		&& pInstance->txBuffers[i].address.word[0] == pAddress.word[0]
		&& pInstance->txBuffers[i].address.word[1] == pAddress.word[1]
		&& pInstance->txBuffers[i].address.word[2] == pAddress.word[2]) {

			tDataBuffer = &(pInstance->txBuffers[i]);

			break;
		}

	}

	return tDataBuffer;
}


struct MHWDataBuffer * MHW_getAnyUnassignedTxBuffer(struct MHWInstance *pInstance) {

	struct MHWDataBuffer *tDataBuffer = 0;
	uint8_t i;

	for(i=0; i<MHW_NUM_TX_BUFFERS; i++) {

		if(pInstance->txBuffers[i].state == MHW_BUFFER_STATE_WAITING_ASSIGNMENT) {

			tDataBuffer = &(pInstance->txBuffers[i]);

			break;
		}

	}

	return tDataBuffer;
}


BasicReturnCode MHW_send(	struct MHWInstance *pInstance,
							MACAddress pAddress,
							uint8_t *pData,
							uint16_t pDataSize) {
    
    uint8_t i;
    struct MHWDataBuffer *tBuffer = 0;
    
    BasicReturnCode tReturnCode = RETURN_ERROR_SIZE_TOO_BIG;
    
    if(pDataSize <= MHW_MTU_SIZE) {
    
		for(i=0; i< MHW_NUM_TX_BUFFERS; i++) {

			if(pInstance->txBuffers[i].state == MHW_BUFFER_STATE_FREE) {

				tBuffer = &pInstance->txBuffers[i];

				break;
			}
		}

		if(tBuffer != 0) {

			memcpy(tBuffer->data, pData, pDataSize);

			tBuffer->dataSize = pDataSize;

			tBuffer->address = pAddress;

            tBuffer->state = MHW_BUFFER_STATE_WAITING_ASSIGNMENT;
            
            //Attempt to assign buffer immediately
            for(i=0; i<MHW_NUM_TIMESLOTS; i++) {
                
                struct MHWSlotData *iSlot = &pInstance->slot[i];
                
                if(iSlot->operation == MHW_SLOT_OPERATION_UNICAST_SEND
                && iSlot->dataBuffer == 0
                && MAC_equals(tBuffer->address, iSlot->remoteAddress)) {
                    
                    MHW_assignDataBuffer(pInstance, iSlot, tBuffer);
                    break;
                    
                }
            }            
            
			tReturnCode = RETURN_SUCCESS;

		} else {

			//Failed to allocate buffer
			tReturnCode = RETURN_ERROR_OUT_OF_MEMORY;

		}
    }
    
    return tReturnCode;
}


BasicReturnCode MHW_changeState(struct MHWInstance *pInstance,
								uint8_t pNewState) {
    
	BasicReturnCode tReturnCode = RETURN_ERROR;
    
	//Leaving State
	switch(pInstance->state) {
        
		case MHW_STATE_INITIALIZING: {

			//

		}break;
        
        case MHW_STATE_WAITING_CONTROL_PACKET: {
            
//            DEBUG_PIN = 0;
            
        } break;
        
        case MHW_STATE_TREATING_CONTROL_PACKET: {
            
            gCpProcessTime = gFrameTimer;
            
        } break;
        
		default: {

			//

		}break;
	}
    
    pInstance->state = pNewState;
    
	//Entering State
	switch(pNewState) {
        
		case MHW_STATE_IDLE: {
            
			pInstance->state = MHW_STATE_IDLE;

		} break;

		case MHW_STATE_NEXT_CHANNEL: {

            //

		} break;

        case MHW_STATE_WAITING_CONTROL_PACKET: {
            
//            DEBUG_PIN = 1;
            
        } break;
        
		case MHW_STATE_TRANSMITTING_CONTROL_PACKET: {

			//SLR_sendData(pInstance->slr, );

		} break;

        case MHW_STATE_TRANSMITTING_DATA_PACKET: {
            
            gDataTxStartTime = 0;
            
        } break;
        
		default: { 

			tReturnCode = RETURN_SUCCESS;

		}break;

	}

	return tReturnCode;
}


void MHW_updateTimers(	struct MHWInstance *pInstance,
						uint32_t pElapsedMs) {
    
	int8_t i;

    for(i=0; i<MHW_NUM_TX_BUFFERS; i++) {

    	if(pInstance->txBuffers[i].state != MHW_BUFFER_STATE_FREE) {

    		pInstance->txBuffers[i].timeout += pElapsedMs;
            
            if(pInstance->txBuffers[i].timeout > 10000) {
               pInstance->txBuffers[i].state = MHW_BUFFER_STATE_FREE; 
            }

    	}

    }

}


struct MHWDataBuffer * MHW_newRxBuffer(struct MHWInstance *pInstance) {
    
    uint8_t i;
    
    struct MHWDataBuffer *tBuffer = 0;
     
    for(i=0; i< MHW_NUM_RX_BUFFERS; i++) {
        
        if(pInstance->rxBuffers[i].state == MHW_BUFFER_STATE_FREE) {
            
            tBuffer = &pInstance->rxBuffers[i];
            
            break;
        }
    }
  
    return(tBuffer);
} 


struct Channel * MHW_searchChannel( struct MHWInstance *pInstance,
									MACAddress pAddress) {

	struct Channel *tChannel = 0;
	uint8_t i;

	for(i=0; i<MHW_MAX_CHANNELS; i++) {

        struct Channel *iChannel = &(pInstance->channels[i]);
        struct MHWChannelInfo *iChannelInfo = (struct MHWChannelInfo *) iChannel->info;
        
		if(MAC_equals(iChannelInfo->address, pAddress)) {

			tChannel = iChannel;
            break;

		}

	}

	return tChannel;
}


BasicReturnCode MHW_unlistChannelSlot(  struct Channel *pChannel,
										uint8_t pSlotIndex,
										uint8_t pFrequency) {

	BasicReturnCode tReturnCode = RETURN_ERROR_NOT_FOUND;
	uint8_t i;
    uint8_t tNumChannelSlots = 0;

    struct MHWChannelInfo *tChannelInfo = (struct MHWChannelInfo *) pChannel->info;
    
	for(i=0; i<MHW_MAX_CHANNEL_SLOTS; i++) {

		if(tChannelInfo->slots[i].index == pSlotIndex
		&& tChannelInfo->slots[i].frequency == pFrequency) {

			tChannelInfo->slots[i].index = 0;
			tChannelInfo->slots[i].frequency = 0;
			tChannelInfo->slots[i].timeToLive = 0;

			tReturnCode = RETURN_SUCCESS;
		}
        
        if(tChannelInfo->slots[i].timeToLive > 0) {
            tNumChannelSlots++;
        }

	}
    
    //Remove host when its last slot has been unlisted
    if(tNumChannelSlots == 0) {
        
        HostBase_onChannelDestroy(pChannel);
            
        tChannelInfo->address = MAC_nullAddress();
            
        pChannel->type = CHANNEL_TYPE_FREE;
        pChannel->flags = 0;
        pChannel->lastTimestamp = 0;

        memset(tChannelInfo->slots, 0, sizeof(tChannelInfo->slots));
        
    }

	return tReturnCode;
}

BasicReturnCode MHW_listFreeSlot(   struct MHWInstance *pInstance,
                                    uint8_t pIndex,
                                    uint8_t pFrequency) {
    
    BasicReturnCode tReturnCode = RETURN_SUCCESS;
    uint8_t i;
    
    uint8_t tOldestEntryTime = 0;
    struct MHWFreeSlotData *tOldestEntry = 0;
    
    for(i=0; i<MHW_FREE_SLOT_LIST_SIZE; i++) {
        
        struct MHWFreeSlotData *tFreeSlot = &(pInstance->freeSlots[i]);
        
        if(tFreeSlot->index == pIndex
        && tFreeSlot->frequency == pFrequency) {
            
            //This is done to avoid entry duplication
            tFreeSlot->timeSinceLastUpdate = 0;
            tOldestEntry = 0;
            break;
            
        } else {
            
            if(tFreeSlot->timeSinceLastUpdate > tOldestEntryTime
            || tOldestEntry == 0) {
                tOldestEntryTime = tFreeSlot->timeSinceLastUpdate;
                tOldestEntry = tFreeSlot;
            }
            
        }
        
    }
    
    if(tOldestEntry != 0) {
        tOldestEntry->index = pIndex;
        tOldestEntry->frequency = pFrequency;
        tOldestEntry->timeSinceLastUpdate = 0;
    }
    
    return tReturnCode;
}

BasicReturnCode MHW_updateChannel(  struct MHWInstance *pInstance,
                                    MACAddress pRemoteAddress,
									uint8_t pSlotIndex,
									uint8_t pFrequency,
									uint8_t pTTL,
                                    uint32_t pPhase) {

	BasicReturnCode tReturnCode = RETURN_ERROR_OUT_OF_MEMORY;
	uint8_t i;
	uint8_t tOldestSlotVectorIndex = 0xff;
	uint8_t tOldestTTL = 0xff;

    struct Channel *tChannel = MHW_searchChannel(pInstance, pRemoteAddress);

    if(tChannel == 0) {
        
        //Register a new remote host
        tChannel = MHW_newChannel(pInstance, pRemoteAddress);
        
        HostBase_onChannelCreate(tChannel);
        
    }

    if(tChannel != 0) {
    
        struct MHWChannelInfo *tChannelInfo = (struct MHWChannelInfo *) tChannel->info;
        
        tChannel->lastTimestamp = TMR_getCurrentTimestamp();
        
        for(i=0; i<MHW_MAX_CHANNEL_SLOTS; i++) {

            if(tChannelInfo->slots[i].index == pSlotIndex
            && tChannelInfo->slots[i].frequency == pFrequency) {

                tChannelInfo->slots[i].index = pSlotIndex;
                tChannelInfo->slots[i].frequency = pFrequency;
                tChannelInfo->slots[i].timeToLive = pTTL;
                tChannelInfo->slots[i].phase = pPhase;
                                
                tReturnCode = RETURN_SUCCESS;
                break;

            } else {

                if(tChannelInfo->slots[i].timeToLive < tOldestTTL) {
                    tOldestTTL = tChannelInfo->slots[i].timeToLive;
                    tOldestSlotVectorIndex = i;
                }

            }

        }
        
        //No entry yet, create a new one over an older one
        if(tReturnCode != RETURN_SUCCESS
        && tOldestSlotVectorIndex < MHW_MAX_CHANNEL_SLOTS) {

            tChannelInfo->slots[tOldestSlotVectorIndex].index = pSlotIndex;
            tChannelInfo->slots[tOldestSlotVectorIndex].frequency = pFrequency;
            tChannelInfo->slots[tOldestSlotVectorIndex].timeToLive = pTTL;
            tChannelInfo->slots[tOldestSlotVectorIndex].phase = pPhase;
            
            tReturnCode = RETURN_SUCCESS;
            
        }
        
    }
    
	return tReturnCode;

}


BasicReturnCode MHW_deleteChannel(  struct MHWInstance *pInstance,
                                    MACAddress pAddress) {
    
    BasicReturnCode tReturnCode = RETURN_ERROR_NOT_FOUND;
    uint8_t i;
    
    for(i=0; i<MHW_MAX_CHANNELS; i++) {
        
        struct Channel *tChannel = &(pInstance->channels[i]);
        struct MHWChannelInfo *tChannelInfo = (struct MHWChannelInfo *) tChannel->info;
        
        if(MAC_equals(tChannelInfo->address, pAddress)) {
           
            HostBase_onChannelDestroy(tChannel);
            
            tChannelInfo->address = MAC_nullAddress();
            
            tChannel->type = CHANNEL_TYPE_FREE;
            tChannel->flags = 0;
            tChannel->lastTimestamp = 0;

            memset(tChannelInfo->slots, 0, sizeof(tChannelInfo->slots));

            tReturnCode = RETURN_SUCCESS;
            break;
           
       } 
       
    }
    
    return tReturnCode;    
}


BasicReturnCode MHW_flushChannels(struct MHWInstance *pInstance) {
    
    BasicReturnCode tReturnCode = RETURN_SUCCESS;
    
    uint8_t i;
    
    for(i=0; i<MHW_MAX_CHANNELS; i++) {
        
        struct Channel *tChannel = &(pInstance->channels[i]);
        struct MHWChannelInfo *tChannelInfo = (struct MHWChannelInfo *) tChannel->info;
        
        if(tChannel->type != CHANNEL_TYPE_FREE) {
        
            HostBase_onChannelDestroy(tChannel);
        
            pInstance->channels[i].type = CHANNEL_TYPE_FREE;
            pInstance->channels[i].flags = 0;
            pInstance->channels[i].lastTimestamp = 0;

            tChannelInfo->address = MAC_nullAddress();

            memset(tChannelInfo->slots, 0, sizeof(tChannelInfo->slots));
            
        }
        
    }
    
    return tReturnCode;
}


BasicReturnCode MHW_countChannelSlots(  struct MHWInstance *pInstance,
                                        uint16_t *pSyncedSlots,
                                        uint16_t *pOutOfSyncSlots) {
    
    BasicReturnCode tReturnCode = RETURN_SUCCESS;
    
    uint8_t i, j;
    
    uint16_t tSyncedSlots = 0;
    uint16_t tOutOfSyncSlots = 0;
    
    for(i=0; i<MHW_MAX_CHANNELS; i++) {
        
        struct Channel *tChannel = &(pInstance->channels[i]);
        struct MHWChannelInfo *tChannelInfo = (struct MHWChannelInfo *) tChannel->info;
        
        if(!MAC_isNull(tChannelInfo->address)) {
            
            for(j=0; j<MHW_MAX_CHANNEL_SLOTS; j++) {

                if(tChannelInfo->slots[j].timeToLive > 0) {
                    
                    if(abs(tChannelInfo->slots[j].phase) < MHW_MAX_TIME_PHASE) {
                        tSyncedSlots++;
                    } else {
                        tOutOfSyncSlots++;
                    }
                    
                }
                
            }
            
        }
        
    }
    
    if(pSyncedSlots != 0) {
        *pSyncedSlots = tSyncedSlots;
        
    }
    
    if(pOutOfSyncSlots != 0) {
        *pOutOfSyncSlots = tOutOfSyncSlots;
    }
    
    return tReturnCode;
    
}


//BasicReturnCode MHW_getPhaseStatistics( struct MHWInstance *pInstance,
//                                        int32_t *pMeanPhase,
//                                        uint32_t *pVariance) {
//    
//    BasicReturnCode tReturnCode;
//    
//    uint8_t i, j;
//    
//    int32_t tMeanPhase = 0;
//    uint32_t tVariance = 0;
//    
//    uint16_t tSyncedSlots;
//    uint16_t tOutOfSyncSlots;
//    uint16_t tTotalSlots;
//    
//    MHW_countKnownHostsSlots(pInstance, &tSyncedSlots, &tOutOfSyncSlots);
//    
//    tTotalSlots = tSyncedSlots + tOutOfSyncSlots;
//    
//    //Get mean phase
//    for(i=0; i<MHW_MAX_KNOWN_HOSTS; i++) {
//        
//        struct MHWRemoteHostData *tRemoteHost = &pInstance->knownHosts[i];
//        
//        if(!MAC_isNull(tRemoteHost->address)) {
//            
//            for(j=0; j<MHW_MAX_KNOWN_HOSTS_SLOTS; j++) {
//
//                if(tRemoteHost->slots[j].timeToLive > 0) {
//                    
//                    tMeanPhase += tRemoteHost->slots[j].timestampFracPhase / tTotalSlots;
//                     
//                }
//                
//            }
//            
//        }
//        
//    }
//    
//    //Get phase variance
//    for(i=0; i<MHW_MAX_KNOWN_HOSTS; i++) {
//        
//        struct MHWRemoteHostData *tRemoteHost = &pInstance->knownHosts[i];
//        
//        if(!MAC_isNull(tRemoteHost->address)) {
//            
//            for(j=0; j<MHW_MAX_KNOWN_HOSTS_SLOTS; j++) {
//
//                if(tRemoteHost->slots[j].timeToLive > 0) {
//                    
//                    int32_t tDelta = tRemoteHost->slots[j].timestampFracPhase - tMeanPhase;
//                    
//                    tDelta /= 32768;    //shift to fit type size
//                    
//                    tVariance += (tDelta * tDelta) / (tTotalSlots + 1);
//                     
//                }
//                
//            }
//            
//        }
//        
//    }
//    
//    if(pMeanPhase != 0) {
//        *pMeanPhase = tMeanPhase;
//    }
//    
//    if(pVariance != 0) {
//        *pVariance = tVariance;
//    }
//    
//    return tReturnCode;
//    
//}


BasicReturnCode MHW_buildControlPacket(	struct MHWInstance *pInstance,
										struct MHWSlotData *pSlotData) {

	DoubleWord tDoubleWord;
	Word tCRC;

	pInstance->cpBuffer[0]  = pInstance->mac.byte[5];
	pInstance->cpBuffer[1]  = pInstance->mac.byte[4];
	pInstance->cpBuffer[2]  = pInstance->mac.byte[3];
	pInstance->cpBuffer[3]  = pInstance->mac.byte[2];
	pInstance->cpBuffer[4]  = pInstance->mac.byte[1];
	pInstance->cpBuffer[5]  = pInstance->mac.byte[0];

	pInstance->cpBuffer[6]  = pSlotData->remoteAddress.byte[5];
	pInstance->cpBuffer[7]  = pSlotData->remoteAddress.byte[4];
	pInstance->cpBuffer[8]  = pSlotData->remoteAddress.byte[3];
	pInstance->cpBuffer[9]  = pSlotData->remoteAddress.byte[2];
	pInstance->cpBuffer[10] = pSlotData->remoteAddress.byte[1];
	pInstance->cpBuffer[11] = pSlotData->remoteAddress.byte[0];

    if(pSlotData->nextSlotIndex < 20) {
        
        struct MHWSlotData *tNextSlot = &(pInstance->slot[pSlotData->nextSlotIndex]);
    
        pInstance->cpBuffer[12] = pSlotData->nextSlotIndex;
        pInstance->cpBuffer[13] = tNextSlot->frequency;
        
    } else {
    
        pInstance->cpBuffer[12] = 0;
        pInstance->cpBuffer[13] = 0;
        
    }
    
	pInstance->cpBuffer[14] = 0;
	pInstance->cpBuffer[15] = 0;
    
	tDoubleWord.dword = pInstance->timestampFrac;
	pInstance->cpBuffer[16] = tDoubleWord.byte[3];
	pInstance->cpBuffer[17] = tDoubleWord.byte[2];
	pInstance->cpBuffer[18] = tDoubleWord.byte[1];
	pInstance->cpBuffer[19] = tDoubleWord.byte[0];

	pInstance->cpBuffer[20] = pSlotData->operation;
    pInstance->cpBuffer[21] = pSlotData->timeToLive;
    
	tDoubleWord.dword = pSlotData->totalRxBytes;
	pInstance->cpBuffer[22] = tDoubleWord.byte[3];
	pInstance->cpBuffer[23] = tDoubleWord.byte[2];
	pInstance->cpBuffer[24] = tDoubleWord.byte[1];
	pInstance->cpBuffer[25] = tDoubleWord.byte[0];

	pInstance->cpBuffer[26] = pSlotData->lastRSSI;
	pInstance->cpBuffer[27] = pSlotData->dataRate;

	tCRC.word = CRC_calculateCRC16(pInstance->cpBuffer, 28);
	pInstance->cpBuffer[28] = tCRC.byte[1];
	pInstance->cpBuffer[29] = tCRC.byte[0];

	return RETURN_SUCCESS;
}


BasicReturnCode MHW_checkControlPacket(	uint8_t *pDataBuffer,
										uint16_t pDataSize) {

	BasicReturnCode tReturnCode = RETURN_ERROR_INVALID_MESSAGE_SIZE;

	Word tCalculatedCRC, tReceivedCRC;

	if(pDataSize == 30) {

		tReceivedCRC.byte[1] = pDataBuffer[28];
		tReceivedCRC.byte[0] = pDataBuffer[29];

		tCalculatedCRC.word = CRC_calculateCRC16(pDataBuffer, 28);

		if(tCalculatedCRC.word == tReceivedCRC.word) {
			tReturnCode = RETURN_SUCCESS;
		} else {
			tReturnCode = RETURN_ERROR_INVALID_CRC;
		}

	}

	return tReturnCode;
}


BasicReturnCode MHW_checkDataPacket(uint8_t *pControlPacket,
                                    uint8_t *pData,
                                    uint16_t pDataSize) {
    
    BasicReturnCode tReturnCode = RETURN_ERROR_INVALID_MESSAGE_SIZE;

	DoubleWord tCalculatedCRC, tReceivedCRC;
    Word tReadDataSize;
    uint8_t tReadFlags;
    
    uint16_t tMinimumDataSize = 6;
            
    if(MAC_isBroadcast(MHW_getControlPacketSourceAddress(pControlPacket))) {
        //Must contain 6 bytes of source address if control packet does not define it
        tMinimumDataSize = 12;
    }
    
    tReadDataSize.byte[1] = pData[0];
    tReadDataSize.byte[0] = pData[1];
    
    if (tReadDataSize.word == pDataSize
    && pDataSize > tMinimumDataSize) {

        tCalculatedCRC.dword = CRC_initCRC32();
        tCalculatedCRC.dword = CRC_updateCRC32(tCalculatedCRC.dword, pControlPacket, 30);
        tCalculatedCRC.dword = CRC_updateCRC32(tCalculatedCRC.dword, pData, pDataSize-4);
        tCalculatedCRC.dword = CRC_finalCRC32(tCalculatedCRC.dword);

        tReceivedCRC.byte[3] = pData[pDataSize-4];
        tReceivedCRC.byte[2] = pData[pDataSize-3];
        tReceivedCRC.byte[1] = pData[pDataSize-2];
        tReceivedCRC.byte[0] = pData[pDataSize-1];

        if(tCalculatedCRC.dword == tReceivedCRC.dword) {
            tReturnCode = RETURN_SUCCESS;
        } else {
            tReturnCode = RETURN_ERROR_INVALID_CRC;
        }

    } else {
        
        tReturnCode = RETURN_ERROR_INVALID_MESSAGE_SIZE;   
        
    }

	return tReturnCode;    
}


MACAddress MHW_getDataPacketSourceAddress(uint8_t *pHeader, uint8_t *pData) {

    MACAddress tSourceAddress;
    
    tSourceAddress.byte[5] = pHeader[6];
    tSourceAddress.byte[4] = pHeader[7];
    tSourceAddress.byte[3] = pHeader[8];
    tSourceAddress.byte[2] = pHeader[9];
    tSourceAddress.byte[1] = pHeader[10];
    tSourceAddress.byte[0] = pHeader[11];
 
    //No source defined on control packet
    if(MAC_isBroadcast(tSourceAddress)) {
        
        //Use from packet, must be present if packet is valid
        tSourceAddress.byte[5] = pData[2];
        tSourceAddress.byte[4] = pData[3];
        tSourceAddress.byte[3] = pData[4];
        tSourceAddress.byte[2] = pData[5];
        tSourceAddress.byte[1] = pData[6];
        tSourceAddress.byte[0] = pData[7];
        
    }
        
    return tSourceAddress;
}


MACAddress MHW_getControlPacketDestinationAddress(uint8_t *pBuffer) {

	MACAddress tAddress;

	tAddress.byte[0] = pBuffer[5];
	tAddress.byte[1] = pBuffer[4];
	tAddress.byte[2] = pBuffer[3];
	tAddress.byte[3] = pBuffer[2];
	tAddress.byte[4] = pBuffer[1];
	tAddress.byte[5] = pBuffer[0];

	return tAddress;
}


MACAddress MHW_getControlPacketSourceAddress(uint8_t *pBuffer) {

	MACAddress tAddress;

	tAddress.byte[0] = pBuffer[11];
	tAddress.byte[1] = pBuffer[10];
	tAddress.byte[2] = pBuffer[9];
	tAddress.byte[3] = pBuffer[8];
	tAddress.byte[4] = pBuffer[7];
	tAddress.byte[5] = pBuffer[6];

	return tAddress;
}


uint32_t MHW_getControlPacketRxBytes(uint8_t *pBuffer) {

	DoubleWord tRxBytes;

	tRxBytes.byte[3] = pBuffer[22];
	tRxBytes.byte[2] = pBuffer[23];
	tRxBytes.byte[1] = pBuffer[24];
	tRxBytes.byte[0] = pBuffer[25];

	return tRxBytes.dword;
}


uint8_t MHW_getControlPacketNextIndex(uint8_t *pBuffer) {

	return *(pBuffer+12);

}

uint8_t MHW_getControlPacketNextFrequency(uint8_t *pBuffer) {

	return *(pBuffer+13);

}


uint8_t MHW_getControlPacketTimeToLive(uint8_t *pBuffer) {

	return *(pBuffer+21);

}


uint32_t MHW_getControlPacketTimestampFrac(uint8_t *pBuffer) {

    DoubleWord tTimestampFrac;
    
    tTimestampFrac.byte[3] = pBuffer[16];
    tTimestampFrac.byte[2] = pBuffer[17];
    tTimestampFrac.byte[1] = pBuffer[18];
    tTimestampFrac.byte[0] = pBuffer[19];
    
    return tTimestampFrac.dword;
}

